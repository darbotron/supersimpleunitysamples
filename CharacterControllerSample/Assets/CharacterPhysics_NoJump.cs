﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//////////////////////////////////////////////////////////////////////////
public class CharacterPhysics_NoJump : MonoBehaviour
{
	public Camera	m_GameplayControlCamera;

	[Space()]
	[Header( "Control" )]
	[Range( 1.0f, .5f )]
	public float	m_GroundRaycastAdditionalHeightProportion;

	[Space()]
	[Header( "Gameplay" )]
	[Range( 1f, 1000f )]
	public float	m_MoveForce;
	[Range( 0f, 1f )]
	public float	m_MoveForceInAirReduction;
	[Range( 0f, 10f )]
	public float	m_LinearDragCoefficient;
	[Range( 0f, 10f )]
	public float	m_SquaredDragCoefficient;


	// you can set this up in the editor or search for it in the editor
	Rigidbody		m_cRigidBody;
	CapsuleCollider	m_cCollider;

	enum EState
	{
		OnFloor,
		InAir
	}

	EState			m_eState = EState.OnFloor;

	//////////////////////////////////////////////////////////////////////////
	void Start()
	{
		m_cRigidBody	= GetComponentInChildren< Rigidbody >( true );
		m_cCollider		= GetComponentInChildren< CapsuleCollider >( true );
	}
	
	//////////////////////////////////////////////////////////////////////////
	void FixedUpdate()
	{
		Vector3 v2ControlDirectionWorldXZ = GetMovementDirectionInWorldXZ();

		float	fRaycastDistance		= ( ( m_cCollider.height / 2f ) * m_GroundRaycastAdditionalHeightProportion );
		Vector3	v3RaycastDirectionWS	= ( Vector3.down * fRaycastDistance );
		Debug.DrawLine( m_cRigidBody.transform.position, ( m_cRigidBody.transform.position + ( v3RaycastDirectionWS * fRaycastDistance ) ), Color.magenta );

		if( Physics.Raycast( m_cRigidBody.transform.position, v3RaycastDirectionWS, fRaycastDistance ) )
		{
			m_eState = EState.OnFloor;
		}
		else
		{
			m_eState = EState.InAir;
		}

		// apply the move force to the character
		float	fCurrentMoveForce	= m_MoveForce;
		float	fCurrentDragForce	= 0f;

		switch( m_eState )
		{
		case EState.OnFloor:
			fCurrentDragForce	=	CalculateDragForce();
			break;
		case EState.InAir:
			fCurrentMoveForce	*=	m_MoveForceInAirReduction;
			break;
		}

		Vector3 v3MoveForce	= ( v2ControlDirectionWorldXZ	* fCurrentMoveForce );
		Vector3 v3DragForce	= ( GetDragDirection()			* fCurrentDragForce );

		m_cRigidBody.AddForce( v3MoveForce );
		m_cRigidBody.AddForce( v3DragForce );
	}

	//////////////////////////////////////////////////////////////////////////
	Vector3 GetDragDirection()
	{
		Vector3 v3DragDirection = -( m_cRigidBody.velocity.normalized );
		return v3DragDirection;
	}

	//////////////////////////////////////////////////////////////////////////
	float CalculateDragForce()
	{
		Vector3 v3DragForce = Vector3.zero;

		// calculate the drag force	to simulate "surface friction"
		// this will: 
		// a) give the character a top speed and 
		// b) create the "feel" of moving them
		// "standard" faked drag force:
		// drag = ( vel * k1 ) + ( ( vel^2 ) + k2 )
		// k1 - "linear drag"
		// k2 - "square drag"
		float fSquaredVelocity	= m_cRigidBody.velocity.sqrMagnitude;
		float fVelocity			= Mathf.Sqrt( fSquaredVelocity );
		float fDragForce		= (		( fVelocity * m_LinearDragCoefficient )
									+	( fSquaredVelocity * m_SquaredDragCoefficient ) );
		return fDragForce;
	}

	//////////////////////////////////////////////////////////////////////////
	Vector3 GetMovementDirectionInWorldXZ()
	{
		Vector3 v3ControlDirectionScreen = Vector3.zero;

		// horizontal control vector
		if( Input.GetKey( KeyCode.A ) )
		{
			v3ControlDirectionScreen.x += ( -1f );
		}
		else if( Input.GetKey( KeyCode.D ) )
		{
			v3ControlDirectionScreen.x += 1f;
		}

		// vertical control vector
		if( Input.GetKey( KeyCode.W ) )
		{
			v3ControlDirectionScreen.y += 1f;
		}
		else if( Input.GetKey( KeyCode.S ) )
		{
			v3ControlDirectionScreen.y += ( -1f );
		}

		// "normalise" the control vector - this makes it have the length 1f.
		// This stops diagonal directions having a longer vector.
		v3ControlDirectionScreen.Normalize();

		{
			Vector3 v3CamerPos = m_GameplayControlCamera.transform.position;
			Debug.DrawLine( v3CamerPos, 
							( v3CamerPos + m_GameplayControlCamera.transform.TransformDirection( v3ControlDirectionScreen * 3f ) ),
							Color.magenta );
		}

		// pro tip:
		// we will be projecting this into the world XZ plane
		// to preserve accuracy we need to choose whichever of the XY or XZ plane of the camera is closest to the world XZ
		// to work this out we compare the camera's Y and Z to the world up and use whichever has the largest dot product
		float fDotWorldYVsCamY = Mathf.Abs( Vector3.Dot( m_GameplayControlCamera.transform.up,		Vector3.up ) );
		float fDotWorldYVsCamZ = Mathf.Abs( Vector3.Dot( m_GameplayControlCamera.transform.forward,	Vector3.up ) );

		// whichever is larger is closer to the same direction
		if( fDotWorldYVsCamY > fDotWorldYVsCamZ )
		{
			// in this case we swap the Y and Z components before transforming the vector into world space as local XZ is closer aligned to world XZ then local XY
			v3ControlDirectionScreen.z	= v3ControlDirectionScreen.y;
			v3ControlDirectionScreen.y	= 0f;
		}

		// convert this from screen space (i.e. camera space ) into world space using the camera's transform
		Vector3 v3ControlDirectionInWorld = m_GameplayControlCamera.transform.TransformDirection( v3ControlDirectionScreen );

		// then we squash it onto the world XZ plane & re-normalise
		v3ControlDirectionInWorld.y = 0f;
		v3ControlDirectionInWorld.Normalize();

		Debug.DrawLine( m_cRigidBody.transform.position, ( m_cRigidBody.transform.position + ( v3ControlDirectionInWorld * 2f ) ), Color.cyan );

		// we need to convert this so that our control direction is 
		return v3ControlDirectionInWorld;
	}
}
