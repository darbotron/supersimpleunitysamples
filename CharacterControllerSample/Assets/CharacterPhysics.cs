﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//////////////////////////////////////////////////////////////////////////
public class CharacterPhysics : MonoBehaviour
{
	public Camera	m_GameplayControlCamera;

	[Space()]
	[Header( "Control" )]
	[Range( 1.0f, 1.5f )]
	public float	m_GroundRaycastAdditionalHeightProportion;

	[Space()]
	[Header( "Gameplay - move" )]
	[Range( 1f, 1000f )]
	public float	m_MoveForce;
	[Range( 0f, 10f )]
	public float	m_LinearDragCoefficient;
	[Range( 0f, 10f )]
	public float	m_SquaredDragCoefficient;

	[Space()]
	[Header( "Gameplay - jumping" )]
	[Range( 1f, 1000f )]
	public float			m_JumpForceMax	= 100f;
	[Range( 0.1f, 3f )]
	public float			m_JumpTimeMax	= 0.5f;
	public AnimationCurve	m_CurveJumpForceOverJumpTime;
	[Range( 0f, 10f )]
	public float			m_LinearDragCoefficientAirY;
	[Range( 0f, 10f )]
	public float			m_SquaredDragCoefficientAirY;
	public bool				m_OnlyApplyDragWhenRising;

	[Space()]
	[Header( "Gameplay - air-steering" )]
	[Range( 0f, 1f )]
	public float			m_MoveForceNotOnGroundReduction;
	[Range( 0f, 10f )]
	public float			m_LinearDragCoefficientAirXZ;
	[Range( 0f, 10f )]
	public float			m_SquaredDragCoefficientAirXZ;

	[Space()]
	[Header( "slopes" )]
	public bool				m_PreventSlidingDownSlopesWithGravity	= false;
	public bool				m_LimitAntiSlideForce					= false;
	[Range( 0.01f, 10f )]
	public float			m_MaxForceToApplyToPreventSliding		= 1f;


	// you can set this up in the editor or search for it in the editor
	Rigidbody		m_cRigidBody;
	CapsuleCollider	m_cCollider;
	float			m_fJumpStartTime				= 0f;
	Vector3			m_v3LastFloorCollisionNormal	= Vector3.zero;

	enum EState
	{
		OnFloor,				// character is touching the floor
		InAirJumping,			// character is actively jumping
		InAir,					// character is in the air but not jumping
		OnFloorJumpCoolDown,	// must have at least one frame of not jumping without the jump button pressed to move back to OnFloor
	}

	EState			m_eState = EState.OnFloor;

	Vector3			m_Control_v3MoveDirection	= Vector3.zero;
	bool			m_Control_bJumpIsPressed	= false;

	//////////////////////////////////////////////////////////////////////////
	void Start()
	{
		m_cRigidBody	= GetComponentInChildren< Rigidbody >( true );
		m_cCollider		= GetComponentInChildren< CapsuleCollider >( true );
	}
	

	//////////////////////////////////////////////////////////////////////////
	// called once per game frame - update logical state of the character here
	//////////////////////////////////////////////////////////////////////////
	void Update()
	{
		// control inputs
		m_Control_v3MoveDirection		= GetMovementDirectionInWorldXZ();
		m_Control_bJumpIsPressed		= Input.GetKey( KeyCode.Space );

		// check instantaneous state
		float	fRaycastDistance		= ( ( m_cCollider.height / 2f ) * m_GroundRaycastAdditionalHeightProportion );
		Vector3	v3RaycastDirectionWS	= ( Vector3.down * fRaycastDistance );

		Debug.DrawLine( m_cRigidBody.transform.position, ( m_cRigidBody.transform.position + ( v3RaycastDirectionWS * fRaycastDistance ) ), Color.magenta );

		RaycastHit	cRaycastHit;
		bool		bTouchingFloor = Physics.Raycast( m_cRigidBody.transform.position, v3RaycastDirectionWS, out cRaycastHit, fRaycastDistance );

		if( bTouchingFloor )
		{
			m_v3LastFloorCollisionNormal = cRaycastHit.normal;
		}

		float		fJumpTimeSoFar	= GetCurrentJumpTime();
		bool		bJumpTimeIsDone	= JumpTimeIsUp();


		// update state based on instantaneous values
		switch( m_eState )
		{
		//--------------------------------------------------------------------
		case EState.OnFloor:
			if(	!bTouchingFloor ) 
			{
				// this test means you have to let go fo jump and press it again bewteen jumps
				if( m_Control_bJumpIsPressed )
				{
					ResetJumpTime();
					m_eState = EState.InAirJumping;
				}
				else
				{
					m_eState = EState.InAir;
				}
			}
			break;

		//--------------------------------------------------------------------
		case EState.InAirJumping:
			if(	bTouchingFloor ) 
			{
				m_eState = EState.OnFloorJumpCoolDown;
			}
			else if( m_Control_bJumpIsPressed && bJumpTimeIsDone )
			{
				m_eState = EState.InAir;
			}
			else if( !m_Control_bJumpIsPressed )
			{
				m_eState = EState.InAir;
			}
			break;

		//--------------------------------------------------------------------
		case EState.InAir:
			if(	bTouchingFloor ) 
			{
				m_eState = EState.OnFloorJumpCoolDown;
			}
			break;

		//--------------------------------------------------------------------
		case EState.OnFloorJumpCoolDown:
			if(	!m_Control_bJumpIsPressed ) 
			{
				m_eState = EState.OnFloor;
			}
			break;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void FixedUpdate()
	{
		// calculate the forces & apply to the character
		//--------------------------------------------------------------------
		float fCurrentMoveForce = m_MoveForce;
		switch( m_eState )
		{
		case EState.InAirJumping:
		case EState.InAir:
			fCurrentMoveForce *= m_MoveForceNotOnGroundReduction;
			break;
		}

		Vector3 v3MoveForce	= ( m_Control_v3MoveDirection * fCurrentMoveForce );
		m_cRigidBody.AddForce( v3MoveForce );

		//--------------------------------------------------------------------
		if( Vector3.zero != m_v3LastFloorCollisionNormal )
		{
			Vector3 v3NotMovingGravityAccelerationParallelToFloorPreventer = Vector3.zero;
			switch( m_eState )
			{
			case EState.OnFloor:
			case EState.OnFloorJumpCoolDown:
				if( m_PreventSlidingDownSlopesWithGravity )
				{ 
					Vector3 v3WeightForce					= ( m_cRigidBody.mass * Physics.gravity ); // f = ma
					float	fDotSurfaceNormalVsWeight		= Vector3.Dot( m_v3LastFloorCollisionNormal, v3WeightForce );
					Vector3 v3WeightSurfaceNormalForce		= ( m_v3LastFloorCollisionNormal * fDotSurfaceNormalVsWeight );
					Vector3 v3WeightForceUnopposedByNormal	= ( v3WeightForce - v3WeightSurfaceNormalForce );

					Debug.DrawLine( m_cRigidBody.transform.position, ( m_cRigidBody.transform.position + v3WeightForceUnopposedByNormal ), Color.yellow );

					Vector3 v3AntiSlideForce				= ( -v3WeightForceUnopposedByNormal );

					if( m_LimitAntiSlideForce )
					{
						float	fAntiSlideForceMagnitude			= v3AntiSlideForce.magnitude;
						float	fAntiSlideForceMagnitudeLimited		= Mathf.Min( fAntiSlideForceMagnitude, m_MaxForceToApplyToPreventSliding );
						Vector3	v3AntiSlideForceNormalised			= ( v3AntiSlideForce / fAntiSlideForceMagnitude );
						v3AntiSlideForce							= ( v3AntiSlideForceNormalised * fAntiSlideForceMagnitudeLimited );
					}

					m_cRigidBody.AddForce( v3AntiSlideForce );
				}
				break;
			}
		}


		//--------------------------------------------------------------------
		Vector3 v3DragForce	= Vector3.zero;
		switch( m_eState )
		{
		case EState.OnFloor:
		case EState.OnFloorJumpCoolDown:
			// note we ignore the vertical component of drag
			v3DragForce	=	GetDragDirection();
			v3DragForce *=	CalculateDragForce( m_LinearDragCoefficient, m_SquaredDragCoefficient );
			break;


		case EState.InAirJumping:
		case EState.InAir:
			// note we treat the horizontal & vertical components of drag separate so that the character can fall faster than they steer
			// this is because the drag for gameplay XZ steering looks very floaty if applied to the Y axis movement
			{
				Vector3 v3DragForceXZ		=	GetDragDirection_WorldXZPlane();
				v3DragForceXZ				*=	CalculateDragForce( m_LinearDragCoefficientAirXZ, m_SquaredDragCoefficientAirXZ );

				Vector3 v3DragForceY		=	GetDragDirection_WorldY();
				
				bool bApplyVerticalAirDrag	= ( m_OnlyApplyDragWhenRising ? ( v3DragForceY.y < 0f ) : true );

				if( bApplyVerticalAirDrag )
				{
					v3DragForceY				*=	CalculateDragForce( m_LinearDragCoefficientAirY, m_SquaredDragCoefficientAirY );
				}
				
				v3DragForce = ( v3DragForceXZ + v3DragForceY );
			}
			break;
		}
		m_cRigidBody.AddForce( v3DragForce );

		//--------------------------------------------------------------------
		float fJumpForce = 0f;
		switch( m_eState )
		{
		case EState.OnFloor:
		case EState.InAirJumping:
			if( m_Control_bJumpIsPressed )
			{
				fJumpForce += m_JumpForceMax;
			}
			break;
		}

		Vector3 v3JumpForce	= ( Vector3.up * fJumpForce );
		m_cRigidBody.AddForce( v3JumpForce );
	}

	//////////////////////////////////////////////////////////////////////////
	void ResetJumpTime()
	{
		m_fJumpStartTime = Time.time;
	}

	//////////////////////////////////////////////////////////////////////////
	float GetCurrentJumpTime()
	{
		return ( Time.time - m_fJumpStartTime );
	}

	//////////////////////////////////////////////////////////////////////////
	bool JumpTimeIsUp()
	{
		return ( GetCurrentJumpTime() >= m_JumpTimeMax );
	}

	//////////////////////////////////////////////////////////////////////////
	float GetJumpForceForTimeSinceJumped()
	{
		float fJumpForceTimeAsPropOfMaxTime	= Mathf.Clamp01( GetCurrentJumpTime() / m_JumpTimeMax );
		float fJumpForcePropOfMaxAttime		= Mathf.Clamp01( m_CurveJumpForceOverJumpTime.Evaluate( fJumpForceTimeAsPropOfMaxTime ) );
		return ( m_JumpForceMax * fJumpForcePropOfMaxAttime );
	}

	//////////////////////////////////////////////////////////////////////////
	Vector3 GetDragDirection_WorldY()
	{
		Vector3 v3DragDirection						= -( m_cRigidBody.velocity.normalized );
		Vector3 v3DragDirection_VerticalComponent	= ( Vector3.up * Vector3.Dot( Vector3.up, v3DragDirection ) );
		return v3DragDirection_VerticalComponent;
	}

	//////////////////////////////////////////////////////////////////////////
	Vector3 GetDragDirection_WorldXZPlane()
	{
		Vector3 v3DragDirection						= -( m_cRigidBody.velocity.normalized );
		Vector3 v3DragDirection_VerticalComponent	= ( Vector3.up * Vector3.Dot( Vector3.up, v3DragDirection ) );
		Vector3 v3DragDirection_NoVerticalComponent	= ( v3DragDirection - v3DragDirection_VerticalComponent );
		return v3DragDirection_NoVerticalComponent;
	}

	//////////////////////////////////////////////////////////////////////////
	Vector3 GetDragDirection()
	{
		Vector3 v3DragDirection	= -( m_cRigidBody.velocity.normalized );
		return v3DragDirection;
	}

	//////////////////////////////////////////////////////////////////////////
	float CalculateDragForce( float fCoefficientLinear, float fCoefficientSquared )
	{
		Vector3 v3DragForce = Vector3.zero;

		// calculate the drag force	to simulate "surface friction"
		// this will: 
		// a) give the character a top speed and 
		// b) create the "feel" of moving them
		// "standard" faked drag force:
		// drag = ( vel * k1 ) + ( ( vel^2 ) + k2 )
		// k1 - "linear drag"
		// k2 - "square drag"
		float fSquaredVelocity	= m_cRigidBody.velocity.sqrMagnitude;
		float fVelocity			= Mathf.Sqrt( fSquaredVelocity );
		float fDragForce		= (		( fVelocity * fCoefficientLinear )
									+	( fSquaredVelocity * fCoefficientSquared ) );
		return fDragForce;
	}

	//////////////////////////////////////////////////////////////////////////
	Vector3 GetMovementDirectionInWorldXZ()
	{
		Vector3 v3ControlDirectionScreen = Vector3.zero;

		// horizontal control vector
		if( Input.GetKey( KeyCode.A ) )
		{
			v3ControlDirectionScreen.x += ( -1f );
		}
		else if( Input.GetKey( KeyCode.D ) )
		{
			v3ControlDirectionScreen.x += 1f;
		}

		// vertical control vector
		if( Input.GetKey( KeyCode.W ) )
		{
			v3ControlDirectionScreen.y += 1f;
		}
		else if( Input.GetKey( KeyCode.S ) )
		{
			v3ControlDirectionScreen.y += ( -1f );
		}

		// "normalise" the control vector - this makes it have the length 1f.
		// This stops diagonal directions having a longer vector.
		v3ControlDirectionScreen.Normalize();

		{
			Vector3 v3CamerPos = m_GameplayControlCamera.transform.position;
			Debug.DrawLine( v3CamerPos, 
							( v3CamerPos + m_GameplayControlCamera.transform.TransformDirection( v3ControlDirectionScreen * 3f ) ),
							Color.magenta );
		}

		// pro tip:
		// we will be projecting this into the world XZ plane
		// to preserve accuracy we need to choose whichever of the XY or XZ plane of the camera is closest to the world XZ
		// to work this out we compare the camera's Y and Z to the world up and use whichever has the largest dot product
		float fDotWorldYVsCamY = Mathf.Abs( Vector3.Dot( m_GameplayControlCamera.transform.up,		Vector3.up ) );
		float fDotWorldYVsCamZ = Mathf.Abs( Vector3.Dot( m_GameplayControlCamera.transform.forward,	Vector3.up ) );

		// whichever is larger is closer to the same direction
		if( fDotWorldYVsCamY > fDotWorldYVsCamZ )
		{
			// in this case we swap the Y and Z components before transforming the vector into world space as local XZ is closer aligned to world XZ then local XY
			v3ControlDirectionScreen.z	= v3ControlDirectionScreen.y;
			v3ControlDirectionScreen.y	= 0f;
		}

		// convert this from screen space (i.e. camera space ) into world space using the camera's transform
		Vector3 v3ControlDirectionInWorld = m_GameplayControlCamera.transform.TransformDirection( v3ControlDirectionScreen );

		// then we squash it onto the world XZ plane & re-normalise
		v3ControlDirectionInWorld.y = 0f;
		v3ControlDirectionInWorld.Normalize();

		Debug.DrawLine( m_cRigidBody.transform.position, ( m_cRigidBody.transform.position + ( v3ControlDirectionInWorld * 2f ) ), Color.cyan );

		// we need to convert this so that our control direction is 
		return v3ControlDirectionInWorld;
	}
}
