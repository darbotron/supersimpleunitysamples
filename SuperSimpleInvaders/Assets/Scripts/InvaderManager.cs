﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvaderManager : MonoBehaviour
{
	public float	m_XAtLeftEdgeOfScreen		= -30f;
	public float	m_XAtRightEdgeOfScreen		= 30f;

	public Vector3	m_InvaderMoveRate			= Vector3.zero;

	public float	m_MoveDownAtEdgeAmount		= -10f;
	public float	m_MoveDownLerpTime			= 0.75f;

	float			m_fMoveDownTargetYPosition	= 0f;
	float			m_fMoveDownVelocity			= 0f;

	List< Invader > m_lstInvaders;

	public void InvaderWasHit( Invader cInvaderWhichWasHit )
	{
		if( m_lstInvaders.Contains( cInvaderWhichWasHit ) )
		{
			m_lstInvaders.Remove( cInvaderWhichWasHit );
			Destroy( cInvaderWhichWasHit.gameObject );
		}
	}

	void Start()
	{
		m_fMoveDownTargetYPosition = transform.position.y;

		m_lstInvaders = new List<Invader>();
		m_lstInvaders.AddRange( GetComponentsInChildren< Invader >( true ) );

		for( int i = 0; i < m_lstInvaders.Count; ++i )
		{
			m_lstInvaders[ i ].SetParentManager( this );
			m_lstInvaders[ i ].SetMoveRate( m_InvaderMoveRate );
		}
	}
	
	void Update()
	{
		// detect any invader touching the edge they're moving towards
		bool bInvadersShouldChangeDirection = false;

		for( int i = 0; i < m_lstInvaders.Count; ++i )
		{
			// if the x component of the move rate is -ve they're moving left
			if( m_InvaderMoveRate.x < 0f )
			{
				if( m_lstInvaders[ i ].transform.position.x < m_XAtLeftEdgeOfScreen )
				{
					bInvadersShouldChangeDirection = true;
					break;
				}
			}
			else
			{
				if( m_lstInvaders[ i ].transform.position.x > m_XAtRightEdgeOfScreen )
				{
					bInvadersShouldChangeDirection = true;
					break;
				}
			}
		}

		if( bInvadersShouldChangeDirection )
		{
			// flip the invader direction
			m_InvaderMoveRate			=	( -m_InvaderMoveRate );

			// set the new target position
			m_fMoveDownTargetYPosition	+=	m_MoveDownAtEdgeAmount;

			for( int i = 0; i < m_lstInvaders.Count; ++i )
			{
				m_lstInvaders[ i ].SetMoveRate( m_InvaderMoveRate );
			}
		}

		// update the vertical target
		// note this is always moving toward a target, but the target only moves when the aliens hit the edge
		float fCurrentYPosition = transform.position.y;
		float fNewYPosition		= Mathf.SmoothDamp( fCurrentYPosition, m_fMoveDownTargetYPosition, ref m_fMoveDownVelocity, m_MoveDownLerpTime );

		Vector3 v3NewPosition	= transform.position;
		v3NewPosition.y			= fNewYPosition;
		transform.position		= v3NewPosition;
	}
}
