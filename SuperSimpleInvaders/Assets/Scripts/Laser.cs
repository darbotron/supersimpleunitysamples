﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Rigidbody ) ) ]
public class Laser : MonoBehaviour
{
	// public
	public float	m_Lifetime		= 1f;
	public Vector3	m_LaserMoveRate	= Vector3.zero;


	// if we don't use 'public' stuff defaults to private'
	float			m_fBirthTime = 0f;
	Rigidbody		m_cRigidBody = null;

	// Use this for initialization
	void Start()
	{	
		m_fBirthTime = Time.time;	
		m_cRigidBody = GetComponent< Rigidbody >();
		m_cRigidBody.velocity = m_LaserMoveRate;
	}
	
	// Update is called once per frame
	void Update()
	{
		if( ( Time.time - m_fBirthTime ) > m_Lifetime )
		{
			Destroy( gameObject );
		}
	}
}
