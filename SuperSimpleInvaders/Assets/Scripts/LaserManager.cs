﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserManager : MonoBehaviour
{
	public GameObject	m_LaserPrefab;

	// spawn and fire a projectile
	public void FireLaserFromPoint( Vector3 v3StartPosition )
	{
		//GameObject cgoLaser = 
			Instantiate( m_LaserPrefab, v3StartPosition, Quaternion.identity );
	}

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
	}
}
