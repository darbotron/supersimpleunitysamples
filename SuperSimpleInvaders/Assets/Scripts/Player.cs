﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public Vector3		m_MoveRateMetersPerSecond	= new Vector3( 20f, 0f, 0f );
	public float		m_MovementRateSmoothTime	= 0.25f;

	public LaserManager	m_LaserManager;
	public Vector3		m_FireLaserOffset			= Vector3.zero;

	Vector3				m_vLastXDistanceChangeRate	= Vector3.zero;
	Vector3				m_vCurrentSmoothingVelocity	= Vector3.zero;
	Rigidbody			m_cRigidbody				= null;

	enum ETouchingSide	{ None, Left, Right };
	ETouchingSide		m_eTouchingSide				= ETouchingSide.None;

	void Start()
	{
		m_cRigidbody = GetComponent< Rigidbody >();
	}

	void OnCollisionEnter( Collision collision )
	{
		HandleCollision( collision );
	}

	void OnCollisionStay( Collision collision )
	{
		HandleCollision( collision );
	}

	void Update()
	{		
		// set the ideal movement rate from user input
		Vector3 vXDistanceChangeRate = Vector3.zero;

		if(		Input.GetKey( UnityEngine.KeyCode.A	) 
			&&	( m_eTouchingSide != ETouchingSide.Left ) )
		{	
			vXDistanceChangeRate = -m_MoveRateMetersPerSecond;
		}

		if(		Input.GetKey( UnityEngine.KeyCode.D	)
			&&	( m_eTouchingSide != ETouchingSide.Right ) )
		{									  
			vXDistanceChangeRate = m_MoveRateMetersPerSecond;
		}		
		
		// smooth the actual movement value over time so it looks nice
		Vector3 v3SmoothedMoveRate	= Vector3.SmoothDamp( m_vLastXDistanceChangeRate, vXDistanceChangeRate, ref m_vCurrentSmoothingVelocity, m_MovementRateSmoothTime );
		m_vLastXDistanceChangeRate	= v3SmoothedMoveRate;
		m_cRigidbody.velocity		= v3SmoothedMoveRate;

		if( Input.GetKeyDown( KeyCode.Space ) )
		{
			Debug.Assert( null != m_LaserManager );
			m_LaserManager.FireLaserFromPoint( transform.position + m_FireLaserOffset );  
		}
	}

	// happens after all other update functions
	void LateUpdate()
	{
		m_eTouchingSide = ETouchingSide.None;
	}

	void HandleCollision( Collision collision )
	{
		if( collision.gameObject.tag == "ScreenBoundLeft" )
		{
			m_eTouchingSide = ETouchingSide.Left;
		}
		else if( collision.gameObject.tag == "ScreenBoundRight" )
		{
			m_eTouchingSide = ETouchingSide.Right;
		}
	}
}
