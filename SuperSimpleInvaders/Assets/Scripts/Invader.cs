﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invader : MonoBehaviour
{
	InvaderManager	m_cParentManager		= null;
	Vector3			m_v3MoveVelocity	= Vector3.zero;

	public void SetParentManager( InvaderManager cParentManager )
	{
		m_cParentManager = cParentManager;
	}

	public void SetMoveRate( Vector3 v3NewMoveVelocity )
	{
		m_v3MoveVelocity = v3NewMoveVelocity;
	}

	void Start()
	{
	}
	
	// Update is called once per frame
	void Update()
	{
		transform.position = transform.position + ( m_v3MoveVelocity * Time.deltaTime );
	}		  

	// consulting the Unity "collision action matrix": https://docs.unity3d.com/Manual/CollidersOverview.html
	// this will only get called if:
	// 1) this object has a collider component with the "Is Trigger?" checkbox ticked AND
	// 2) the object colliding with it (i.e. the laser is a rigidbody with a collider *not* set to trigger
	void OnTriggerEnter( Collider other )
	{
		// check that the object colliding is definitely a laser (though we don't expoect it to collide with anything else)
		if( other.gameObject.tag == "CanDamage" )
		{
			m_cParentManager.InvaderWasHit( this );
		}
	}
}
