﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PointAndClickDriver : MonoBehaviour
{
	public Camera		m_cCamera;
	public NavMeshAgent	m_cNavMeshAgent;

	// Update is called once per frame
	void Update()
	{
		if( Input.GetMouseButtonDown( 0 ) )
		{
			// this takes the current mouse position in screen pixels and uses the camera to convert it into a ray.
			// a ray is a basically a point in space and a direction.
			Ray cMousePositionOnScreenToWorldSpaceRay = m_cCamera.ScreenPointToRay( Input.mousePosition );

			// this debug draw draws the ray in red in the scene view
			Debug.DrawLine( cMousePositionOnScreenToWorldSpaceRay.origin, cMousePositionOnScreenToWorldSpaceRay.origin + ( cMousePositionOnScreenToWorldSpaceRay.direction * 50f ), Color.red, 1f );

			// now we have the ray coming out of the camera through the screen we can test to see if it collides with the floor
			RaycastHit cRaycastHit;
			if( Physics.Raycast( cMousePositionOnScreenToWorldSpaceRay, out cRaycastHit ) )
			{
				// if we get a collision with the floor set the destination for the navmesh agent to the collision point
				// and the navmesh agent will just magically go there
				m_cNavMeshAgent.SetDestination( cRaycastHit.point );
			}		
		}
	}
}
